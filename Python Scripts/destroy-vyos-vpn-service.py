import requests
import json
import sys
import logging
import os
import time
requests.packages.urllib3.disable_warnings()

# Assumptions
# VIM account is already created with name - OpenStackVyOS in OSM portal.
# OSM username - admin, password - admin, project - admin
# NS Name - Site2SiteVPN, NSD Name - vyos_ubuntu_ns
# VNF Package Names - ubuntu_vnf_central, ubuntu_vnf_local, vyos_vnf_central, vyos_vnf_local


osmIp = sys.argv[1]             # OSM portal IP - user input/Jenkins Parameter
url = f"https://{osmIp}:9999/osm"   # OSM NBI host url
vnfpackages = [ "ubuntu_vnf_central", "ubuntu_vnf_local", "vyos_vnf_central", "vyos_vnf_local" ]
nspackage = "vyos_ubuntu_ns"


# function to retrieve token from OSM NBI
def get_token(url):
    payload = json.dumps({ "username": "admin", "password": "admin", "project_id": "admin"})
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        token = response.json()['id']
        print(f"token: {token}")
        return token
    else:
        print("Failed to get token from OSM.")
        sys.exit()


# function to get NS Instance Id from OSM
def query_ns_instances(url, token):
    headers = { 'Authorization': f'Bearer {token}', 'Accept': 'application/json' }
    response = requests.request("GET", url, headers=headers, verify=False)  # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        nsInstanceId = ""
        nsInstances = response.json()
        for nsInstance in nsInstances:
            if nsInstance['name'] == "Site2SiteVPN":
                nsInstanceId = nsInstance['_id']
                print(f"NS Instance Id found for NS Site2SiteVPN: {nsInstanceId}")
                return nsInstanceId
        if not nsInstanceId:
            print("Failed to get NS Instance Id, NS Site2SiteVPN does not exist in OSM.")
            sys.exit()
    else:
        print(f"Failed to get NS Instance Id due to error: {response.text}")
        sys.exit()


# function to Check Instantiate/Terminate NS order status
def get_lcm_operation_status(url, token, operation):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        operationState = response.json()['operationState']
        if operationState == 'FAILED' or operationState == 'COMPLETED':
            print(f"NS {operation} operation {operationState}.")
            return operationState
        else:
            print(f"NS {operation} operation is still in progress, please wait.")
            time.sleep(20) #delay next status check by 20 seconds
            get_lcm_operation_status(url, token, operation) # check status again
    else:
        print(f"Failed to get NS {operation} order status due to error: {response.text}")
        sys.exit()

# function to Terminate NS
def terminate_ns_instance(url, token):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json", "Content-Type": "application/json"}
    payload = json.dumps({})
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 202:
        nsLcmOpOccId = response.json()['id']
        print(f"NS termination started successfully with nsLcmOpOccId: {nsLcmOpOccId}")
        return nsLcmOpOccId
    else:
        print(f"Failed to Terminate NS instance due to error: {response.text}")
        sys.exit()


# function to Delete NS Instance
def delete_ns_instance(url, token):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json"}
    response = requests.request("DELETE", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 204:
        print(f"NS instance deleted successfully.")
    else:
        print(f"Failed to delete NS instance due to error: {response.text}")
        sys.exit()

# function to Get NS Package Id from OSM
def query_ns_descriptor(url, token, nspackage):
    headers = { 'Authorization': f'Bearer {token}', 'Accept': 'application/json' }
    response = requests.request("GET", url, headers=headers, verify=False)  # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        nsdId = ""
        nsds = response.json()
        for nsd in nsds:
            if nsd['name'] == nspackage:
                nsdId = nsd['_id']
                print(f"NS Descriptor Id found for NSD {nspackage}: {nsdId}")
                return nsdId
        if not nsInstanceId:
            print("Failed to get NS Descriptor Id, NSD {nspackage} does not exist in OSM.")
            sys.exit()
    else:
        print(f"Failed to get NS Descriptor Id due to error: {response.text}")
        sys.exit()

# function to delete NS Package
def delete_ns_package(url, token):
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.request("DELETE", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 204:
        print(f"NS package deleted successfully.")
    else:
        print(f"Failed to delete NS package due to error: {response.text}")
        sys.exit()

# function to Get VNF Package Id from OSM
def query_vnf_descriptor(url, token, package):
    headers = { 'Authorization': f'Bearer {token}', 'Accept': 'application/json' }
    response = requests.request("GET", url, headers=headers, verify=False)  # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        vnfdId = ""
        vnfds = response.json()
        for vnfd in vnfds:
            if vnfd['name'] == package:
                vnfdId = vnfd['_id']
                print(f"VNFD Id found for VNFD {package}: {vnfdId}")
                return vnfdId
        if not vnfdId:
            print("Failed to get VNF Descriptor Id, VNFD {package} does not exist in OSM.")
            sys.exit()
    else:
        print(f"Failed to get VNF Descriptor Id due to error: {response.text}")
        sys.exit()


# function to delete VNF Package
def delete_vnf_package(url, token):
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.request("DELETE", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 204:
        print(f"VNF package deleted successfully.")
    else:
        print(f"Failed to delete VNF package due to error: {response.text}")
        sys.exit()


# Execution flow starts from here

# Get token
token = get_token(url+"/admin/v1/tokens")

# Query information about multiple NS instances
nsInstanceId = query_ns_instances(url+"/nslcm/v1/ns_instances", token)

# Terminate NS Instance
nsLcmOpOccId = terminate_ns_instance(url+f"/nslcm/v1/ns_instances/{nsInstanceId}/terminate", token)

# Query NS LCM operatin status
operationState = get_lcm_operation_status(url+f"/nslcm/v1/ns_lcm_op_occs/{nsLcmOpOccId}", token, "Termination")

# Delete NS Instance resource
delete_ns_instance(url+f"/nslcm/v1/ns_instances/{nsInstanceId}", token)

# Query information about multiple NS package resources
nsdInfoId = query_ns_descriptor(url+"/nsd/v1/ns_descriptors_content", token, nspackage)

# Delete an individual NS package resource
delete_ns_package(url+f"/nsd/v1/ns_descriptors_content/{nsdInfoId}", token)

# Query information about multiple VNF package resources
for package in vnfpackages: #Delete all VNF packages one bye one
    packageContentId = query_vnf_descriptor(url+"/vnfpkgm/v1/vnf_packages_content", token, package)
    # Delete an individual VNF package resource
    delete_vnf_package(url+f"/vnfpkgm/v1/vnf_packages_content/{packageContentId}", token)
