import requests
import json
import sys
import logging
import os
import time
requests.packages.urllib3.disable_warnings()

# Assumptions
# VIM account is already created with name - OpenStackVyOS in OSM portal.
# OSM username - admin, password - admin, project - admin

# Get OSM portal IP from Jenkins environment variable
#osmIp = os.getenv('OSM_IP')     # OSM portal IP
# Get OSM portal IP from user - used for unit testing w/o Jenkins
osmIp = sys.argv[1]             # OSM portal IP
url = f"https://{osmIp}:9999/osm"   # OSM NBI host url
package_repository = "https://gitlab.com/api/v4/projects/26733879/repository/files/" # Gitlab repository URL
vnfpackages = ["ubuntu_vnf_central.tar.gz", "ubuntu_vnf_local.tar.gz", "vyos_vnf_central.tar.gz", "vyos_vnf_local.tar.gz"] # VNF Packages
nspackage = "vyos_ubuntu_ns.tar.gz" # NS Package
vnf_package_directory = "VNF%20Packages%2F" # VNF Package directory
ns_package_directory = "NS%20Packages%2F"   # NS Package directory

# function to retrieve token from OSM NBI
def get_token(url):
    payload = json.dumps({ "username": "admin", "password": "admin", "project_id": "admin"})
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        token = response.json()['id']
        print(f"token: {token}")
        return token
    else:
        print("Failed to get token from OSM.")
        sys.exit()

# function to upload VNFD and NSD package to OSM 
def upload_package(url, package_repo, package_name, token):
    # Download the package from repository and save it locally
    # Use headers= {"PRIVATE-TOKEN": "_DedY1-3V5pDHquDGgmW"} for private Gitlab projects; not needed for public projects
    with requests.get(package_repo+"/raw?ref=master", stream=True, verify=False) as response, open(package_name, 'wb') as out_file:        
        if response.status_code == 200:
            for chunk in response.iter_content(chunk_size=1024*1024): # Download package content in chunks 
                out_file.write(chunk)   # Write each chunk to local file.
        else:
            print(f"Failed to download package from repo due to error: {response.text}")
            sys.exit()

    with open(package_name, 'rb') as payload:   # Open locally downloaded package for upload
        headers = {"Authorization": f"Bearer {token}", "Accept": "application/json", "Content-Type": "application/zip"}
        response = requests.request("POST", url, headers=headers, data=payload, verify=False)
        # check if response is sucessful
        if response.status_code == 201:
            package_id = response.json()['id']
            print(f"Package uploaded successfully with package_id: {package_id}")
            return package_id
        else:
            print(f"Failed to upload package due to error: {response.text}")
            sys.exit()

# function to get VIM Account Id from OSM
def get_vim_account_id(url, token):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        vimAccountId = ""
        vims = response.json()
        for vim in vims:
            if vim['name'] == "OpenStackVyOS":
                vimAccountId = vim['_id']
                print(f"VIM Account Id found for VIM OpenStackVyOS: {vimAccountId}")
                return vimAccountId
        if not vimAccountId:
            print("Failed to get VIM Account Id, VIM OpenStackVyOS does not exist in OSM.")
            sys.exit()
    else:
        print(f"Failed to get VIM Account Id due to error: {response.text}")
        sys.exit()

# function to Create NS Instance
def create_ns_instance(url, token, nsdId, vimAccountId):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json", "Content-Type": "application/json"}
    payload = json.dumps({"nsName": "Site2SiteVPN", "nsdId": f"{nsdId}", "vimAccountId": f"{vimAccountId}", "nsDescription": "Created by Robot Automation"})
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 201:
        nsInstanceId = response.json()['id']
        print(f"NS instance created successfully with nsInstanceId: {nsInstanceId}")
        return nsInstanceId
    else:
        print(f"Failed to create NS Instance due to error: {response.text}")
        sys.exit()

# function to Instantiate NS
def instantiate_ns(url, token, nsdId, vimAccountId):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json", "Content-Type": "application/json"}
    payload = json.dumps({"nsName": "Site2SiteVPN", "nsdId": f"{nsdId}", "vimAccountId": f"{vimAccountId}", "nsDescription": "Created by Robot Automation"})
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 202:
        nsLcmOpOccId = response.json()['id']
        print(f"NS instantiation started successfully with nsLcmOpOccId: {nsLcmOpOccId}")
        return nsLcmOpOccId
    else:
        print(f"Failed to Instantiate NS due to error: {response.text}")
        sys.exit()

# function to Check Instantiate/Terminate NS order status
def get_lcm_operation_status(url, token, operation):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 200:
        operationState = response.json()['operationState']
        if operationState == 'FAILED' or operationState == 'COMPLETED':
            print(f"NS {operation} operation {operationState}.")
            return operationState
        else:
            print(f"NS {operation} operation is still in progress, please wait.")
            time.sleep(20) #delay next status check by 20 seconds
            get_lcm_operation_status(url, token, operation) # check status again
    else:
        print(f"Failed to get NS {operation} order status due to error: {response.text}")
        sys.exit()

# function to Terminate NS
def terminate_ns_instance(url, token):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json", "Content-Type": "application/json"}
    payload = json.dumps({})
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 202:
        nsLcmOpOccId = response.json()['id']
        print(f"NS termination started successfully with nsLcmOpOccId: {nsLcmOpOccId}")
        return nsLcmOpOccId
    else:
        print(f"Failed to Terminate NS instance due to error: {response.text}")
        sys.exit()

# function to Delete NS Instance
def delete_ns_instance(url, token):
    headers = {"Authorization": f"Bearer {token}", "Accept": "application/json"}
    response = requests.request("DELETE", url, headers=headers, verify=False)   # verify=false to disable ssl certificate checking
    # check if response is sucessful
    if response.status_code == 204:
        print(f"NS instance deleted successfully.")
    else:
        print(f"Failed to delete NS instance due to error: {response.text}")
        sys.exit()



# Execution flow starts from here

# Get token
token = get_token(url+"/admin/v1/tokens")

# Upload VNF and NS packages
for package in vnfpackages: #upload all VNF packages one bye one
    upload_package(url+"/vnfpkgm/v1/vnf_packages_content", package_repository + vnf_package_directory + package, package, token)

nsdId = upload_package(url+"/nsd/v1/ns_descriptors_content", package_repository + ns_package_directory + nspackage, nspackage, token)

# Get VIM Account Id
vimAccountId = get_vim_account_id(url+"/admin/v1/vims", token)


# Create NS instance resource
nsInstanceId = create_ns_instance(url+"/nslcm/v1/ns_instances", token, nsdId, vimAccountId)

# Instantia NS
nsLcmOpOccId = instantiate_ns(url+f"/nslcm/v1/ns_instances/{nsInstanceId}/instantiate", token, nsdId, vimAccountId)

# Query NS LCM operatin status
operationState = get_lcm_operation_status(url+f"/nslcm/v1/ns_lcm_op_occs/{nsLcmOpOccId}", token, "Instantiation")

# Wait for VMs to get configured on first boot
#time.sleep(60)


# Below lines are related to terminate operation, not needed for main flow
# Terminate NS Instance
#nsLcmOpOccId = terminate_ns_instance(url+f"/nslcm/v1/ns_instances/{nsInstanceId}/terminate", token)

# Query NS LCM operatin status
#operationState = get_lcm_operation_status(url+f"/nslcm/v1/ns_lcm_op_occs/{nsLcmOpOccId}", token, "Termination")

# Delete NS Instance resource
#delete_ns_instance(url+f"/nslcm/v1/ns_instances/{nsInstanceId}", token)
