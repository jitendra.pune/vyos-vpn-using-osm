*** Settings ***
Suite Setup       Open Connection And Log In
Suite Teardown    Close All Connections
Library           SSHLibrary
Library           String
Library           Collections

*** Variables ***
${osControllerIP}    0.0.0.0
${osUser}         openstack
${osPassword}     Kubernetes@1234
${vyosUser}       osm
${vyosPassword}    osm21
${ubuntuUser}     ubuntu
${ubuntuPassword}    osm21
${bypassHostChecking}    -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
${regex}          (?m)sender|receiver

*** Test Cases ***
1. Verify DHCP Leases on Both VyOS Routers
    [Tags]    Ready
    Write    ssh ${bypassHostChecking} ${vyosUser}@${vyosCentral}
    Read Until    password:
    Write    ${vyosPassword}
    Read Until Prompt
    Write    show dhcp server leases
    ${output}=    Read Until Prompt
    Should Contain    ${output}    EndhostCB
    Log    DHCP Lease is present for EndhostCB on VyoS-Central-Branch
    Write    exit
    Read Until Prompt
    Write    ssh ${bypassHostChecking} ${vyosUser}@${vyosLocal}
    Read Until    password:
    Write    ${vyosPassword}
    Read Until Prompt
    Write    show dhcp server leases
    ${output}=    Read Until Prompt
    Should Contain    ${output}    EndhostLB
    Log    DHCP Lease is present for EndhostLB on VyoS-Local-Branch
    Write    exit
    Read Until Prompt

2. Verify VPN Connectivity with ping from one end-host to another
    [Tags]    Ready
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuCentral}
    Read Until    password:
    Write    ${ubuntuPassword}
    Read Until Prompt
    Set Client Configuration    prompt=REGEXP:${ubuntuUser}@.*\\$
    Write    sudo ip netns exec vpn ifconfig ens4
    Read Until Prompt
    Write    sudo ip netns exec vpn hostname -I
    ${source}=    Read Until Prompt    strip_prompt=True
    Should Not Be Empty    ${source}
    Log    IP address is present for EndhostCB.
    Set Suite Variable    \${source}
    Write    exit
    Set Client Configuration    prompt=$
    Read Until Prompt
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuLocal}
    Read Until    password:
    Write    ${ubuntuPassword}
    Read Until Prompt
    Set Client Configuration    prompt=REGEXP:${ubuntuUser}@.*\\$
    Write    sudo ip netns exec vpn ifconfig ens4
    Read Until Prompt
    Write    sudo ip netns exec vpn hostname -I
    ${destination}=    Read Until Prompt    strip_prompt=True
    Should Not Be Empty    ${destination}
    Log    IP address is present for EndhostLB.
    Set Suite Variable    \${destination}
    Write    sudo ip netns exec vpn ping -c 5 ${source}
    #Read    delay=2s
    #Write    sudo ip netns exec vpn ping -c 1 ${source}
    ${output}=    Read Until Prompt    
    #Should Contain    ${output}    0% packet loss
    Should Contain    ${output}    64 bytes from
    Log    VPN ping successful.    
    Set Client Configuration    prompt=REGEXP:${osUser}@.*\\$
    Write    exit
    Read Until Prompt

3. Verify Firewall Rule to block SSH
    [Tags]    Ready
    Write    ssh ${bypassHostChecking} ${vyosUser}@${vyosCentral}
    Read Until    password:
    Write    ${vyosPassword}
    Set Client Configuration    prompt=$
    Read Until Prompt
    Write    clear firewall name BLOCK_SSH counters
    Read Until Prompt
    Write    clear firewall name BLOCK_SSH rule 10 counters
    Read Until Prompt
    Log    Firewall rull counters are cleared on VyoS-Central-Branch
    Set Client Configuration    prompt=REGEXP:${osUser}@.*\\$
    Write    exit
    Read Until Prompt
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuCentral}
    Read Until    password:
    Write    ${ubuntuPassword}
    Set Client Configuration    prompt=$
    Read Until Prompt
    Write    sudo ip netns exec vpn ssh ${destination}
    Log    Starting SSH connection from EndhostCB to EndhostLB.
    Sleep	1s    
    Set Client Configuration    prompt=REGEXP:${osUser}@.*\\$
    Write	\~.
    Read Until Prompt
    Write    ssh ${bypassHostChecking} ${vyosUser}@${vyosCentral}
    Read Until    password:
    Write    ${vyosPassword}
    Set Client Configuration    prompt=REGEXP:${vyosUser}@.*\\$
    Read Until Prompt    
    Write    show firewall name BLOCK_SSH statistics | grep DROP | awk '{print $3}'
    ${output}=    Read Until Prompt    strip_prompt=True    
    Should Not Be Equal As Integers    ${output}    0
    Log    SSH is blocked as per Firewall rule statistics.
    Log    ${output}
    Set Client Configuration    prompt=REGEXP:${osUser}@.*\\$
    Write    exit
    Read Until Prompt


4. Verify Source NAT
    [Tags]    Ready
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuCentral}
    Read Until    password:
    Write    ${ubuntuPassword}
    Set Client Configuration    prompt=REGEXP:${ubuntuUser}@.*\\$
    Read Until Prompt    
    Write    sudo ip netns exec vpn ping -c 5 8.8.8.8
    ${output}=    Read Until Prompt
    Write    exit
    Set Client Configuration    prompt=$
    Read Until Prompt
    Write    ssh ${bypassHostChecking} ${vyosUser}@${vyosCentral}
    Read Until    password:
    Write    ${vyosPassword}
    Read Until Prompt
    Set Client Configuration    prompt=REGEXP:${vyosUser}@.*\\$
    Write    show nat source translations detail | grep 8.8.8.8 | awk '{print $1 " " $3}'
    ${output}=    Read Until Prompt    strip_prompt=True
    #${endhost}=    Strip String    ${source}    characters=\n
    Should Contain    ${output}    ${vyosCentral}
    Should Contain    ${output}    ${source}    strip_spaces=True
    Log    ${output}
    Write    exit
    Set Client Configuration    prompt=$
    Read Until Prompt
    
5. Verify Rate Limiter policy for VPN Traffic
    [Tags]    Ready
    Open Connection    ${osControllerIP}    prompt=$    alias=ubuntuCentral    timeout=60 seconds
    Login    ${osUser}    ${osPassword}
    Switch Connection    default
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuCentral}
    Read Until    password:
    Write    ${ubuntuPassword}
    Read Until Prompt
    Write    sudo ip netns exec vpn iperf3 -s
    Switch Connection    ubuntuCentral
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${ubuntuLocal}
    Read Until    password:
    Write    ${ubuntuPassword}
    Read Until Prompt
    Write    sudo ip netns exec vpn iperf3 -c ${source}
    ${clientoutput}=    Read Until Prompt
    Log    ${clientoutput}
    Switch Connection    default
    ${serveroutput}=    Read
    Log    ${serveroutput}
    ${outputLines}=    Get Lines Matching Regexp    ${clientoutput}    ${regex}    partial_match=True
    ${bitrateText}=    Get Regexp Matches    ${outputLines}    (\\d)+\\.(\\d+) Mbits/sec
    #${bitrateText}=    Set Variable    ['2.00 Mbits/sec', '1.91 Mbits/sec']
    #${bitrate}=    Get Regexp Matches    ${bitrateText}    (\\d)+\\.(\\d+)
    FOR    ${bitrate}    IN    @{bitrateText}
        #${status}=    Evaluate    ${speed} <= 2
        #Log    ${status}
        ${speed}=    Get Regexp Matches    ${bitrate}    (\\d)+\\.(\\d+)
        Should Be True    ${speed[0]} <= 2
    END

*** Keywords ***
Open Connection And Log In
    Set Default Configuration    timeout=60 seconds
    Open Connection    ${osControllerIP}    prompt=$    alias=default
    Login    ${osUser}    ${osPassword}
    Get VM IPs From OpenStack
    Wait For VMs To Be Fully Up And Configured
    Switch Connection    default
    
    
Get VM IPs From OpenStack
    Log    "Inside Get VM IPs From OpenStack"
    Set Client Configuration    prompt=REGEXP:${osUser}@.*\\$
    Write    sudo ifconfig br-ex 172.24.4.1
    Read Until Prompt
    Write    sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE    
    Read Until Prompt
    Write    source admin-openrc.sh
    Read Until Prompt
    Write    ./getVmIps.sh
    ${stdout}=    Read Until Prompt    strip_prompt=True
    Log    ${stdout}
    ${vmipDict}    Evaluate    json.loads('''${stdout}''')    json
    ${ubuntuCentral}=    Get From Dictionary    ${vmipDict}    ubuntu-central
    Set Suite Variable    ${ubuntuCentral}
    ${ubuntuLocal}=    Get From Dictionary    ${vmipDict}    ubuntu-local
    Set Suite Variable    ${ubuntuLocal}
    ${vyosCentral}=    Get From Dictionary    ${vmipDict}    vyos-central
    Set Suite Variable    ${vyosCentral}
    ${vyosLocal}=    Get From Dictionary    ${vmipDict}    vyos-local
    Set Suite Variable    ${vyosLocal}
    Log Many    ${ubuntuCentral}    ${ubuntuLocal}    ${vyosCentral}    ${vyosLocal}
    Set Client Configuration    prompt=$

Wait For VMs To Be Fully Up And Configured
    @{servers}=    Set Variable    ${ubuntuLocal}    ${vyosLocal}    ${vyosCentral}    ${ubuntuCentral}
    FOR    ${host}    IN    @{servers}
        Wait Until Keyword Succeeds    5 min    1 min    Check VM Ping Status    ${host}
    END
    @{ubuntuservers}=    Set Variable    ${ubuntuLocal}    ${ubuntuCentral}
    FOR    ${host}    IN    @{ubuntuservers}
        Wait Until Keyword Succeeds    5 min    1 min    Check VM cloud-init status    ${host}
    END
    

Check VM Ping Status
    [Arguments]    ${host}
    Write    ping -c 1 ${host}
    ${output}=    Read Until Prompt
    Should Contain    ${output}    64 bytes from

Check VM cloud-init status
    [Arguments]    ${host}
    Open Connection    ${osControllerIP}    prompt=$    alias=statuscheck
    Login    ${osUser}    ${osPassword}
    Write    ssh ${bypassHostChecking} ${ubuntuUser}@${host}
    Read Until    password:
    Write    ${ubuntuPassword}
    Read Until Prompt
    Write    systemctl list-jobs
    Set Client Configuration    prompt=REGEXP:${ubuntuUser}@.*\\$
    ${output}=    Read Until Prompt    strip_prompt=True
    Should Contain    ${output}    No jobs running.
    Close Connection
    

    #################test.sh#######version#1####
    #for id in    $(openstack server list --name Site2SiteVPN* | awk '{ if(length($2) > 4) print $2}');
    #do
    #    instance_name=$(openstack server show $id | grep "^| name    " | awk -F '-' '{print $3"-"$4}');
    #    address=$(openstack server show $id | grep "^| addresses    " | awk '{print $5}' | sed 's/.$//');
    #    json+=($instance_name);
    #    json+=($address);
    #done
    #printf '{';
    #printf '"%s": "%s",' "${json[@]}" | sed s'/.$//';
    #printf '}\n';
    #################test.sh#######version#2####
    #for id in    $(openstack server list --name Site2SiteVPN* | awk '{ if(length($2) > 4) print $2}');
    #do
    #instance_name=$(openstack server show $id | grep "^| name    " | awk -F '-' '{print $3"-"$4}');
    #address=$(openstack server show $id | grep -Po "172.(\d+\.){2}\d+");
    #json+=($instance_name);
    #json+=($address);
    #done
    #printf '{';
    #printf '"%s": "%s",' "${json[@]}" | sed s'/.$//';
    #printf '}\n';
    #######admin-openrc.sh###########
    #export OS_USERNAME=admin
    #export OS_PASSWORD=password
    #export OS_TENANT_NAME=admin
    #export OS_PROJECT_DOMAIN_ID=default
    #export OS_USER_DOMAIN_ID=default
    #export OS_AUTH_URL=http://172.16.5.47/identity/v3/
    #export OS_PROJECT_DOMAIN_NAME=default
    #export OS_IDENTITY_API_VERSION=3
