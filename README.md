**Assumptions**
1. OSM and OpenStack are up and running.
2. OSM is setup with default username, password and project i.e. admin. If not then python script needs to be updated with correct values.
3. VIM account is already created with name - "OpenStackVyOS" having configuration containing ssh key.
4. VyOS and Ubuntu cloud-init images are already uploaded in OpenStack.
    VyOS image can be found at - http://osm-download.etsi.org/ftp/osm-6.0-six/7th-hackfest/images/vyos-1.1.7-cloudinit.qcow2 
    Ubuntu image can be downloaded from - https://cloud-images.ubuntu.com/releases/disco/release-20200109/ubuntu-19.04-server-cloudimg-amd64.img (RAW) 
5. Management network "external" is already created.

**Contents**
1. Python Scripts folder contains script to download VNF and NS packages, onboard them on OSM and NS instantiation.
Execute python script locally using below command.
`python <filename>.py <osm ip>`

Make sure line `osmIp = sys.argv[1]` is uncommented and `osmIp = os.getenv('OSM_IP')` is commented out.

2. Robot Tests folder contains automations script to validate VPN service.
Execute robot file locally using below command.
`python3 -m robot --variable osControllerIP:<OpenStack Controller Node IP> <filename>.robot`

3. VNF packages folder contains four VNF descriptors.
4. NS Package folder contains NS descriptor for VPN service.



